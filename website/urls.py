from django.urls import path
from . import views


app_name = "website"
urlpatterns = [
    path("", views.IndexView.as_view(), name="home"),
    # path("about/", views.AboutView.as_view(), name="about"),
    # path("blogs/", views.BlogView.as_view(), name="blogs"),
    # path("contact/", views.ContactView.as_view(), name="contact"),
    # path("gallery/", views.GalleryView.as_view(), name="gallery"),
    # path("services/", views.ServicesView.as_view(), name="services"),
]
